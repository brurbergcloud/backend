CREATE TABLE Entries (
  ID SERIAL NOT NULL UNIQUE,
  Domain TEXT NOT NULL,
  Level SMALLINT NOT NULL CHECK (Level >= 0),
  Context TEXT,
  Message TEXT NOT NULL,
  ErrorTime TIMESTAMPTZ NOT NULL,
  ConsentedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE INDEX Entries_Domain_Context ON Entries
(
    Domain,
    Context
);
